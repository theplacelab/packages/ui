var path = require("path");
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const WebpackShellPlugin = require("webpack-shell-plugin");

module.exports = (env, argv) => {
  const mode = argv.mode;
  const postBuildScript =
    process.env.YALC === "true" ? "npx yalc push" : "true";
  let plugins = argv.addon === "analyze" ? [new BundleAnalyzerPlugin()] : [];
  plugins.push(new WebpackShellPlugin({ onBuildExit: postBuildScript }));

  console.log(`Environment: ${mode}`);
  return {
    plugins,
    entry: "./src/index.js",
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "[name].js",
      library: "index",
      globalObject: "this",
      libraryTarget: "commonjs2",
    },
    externals: {
      webpack: "commonjs webpack",
      "@babel": "commonjs @babel",
      react: "commonjs react",
      "prop-types": "commonjs prop-types",
      "@fortawesome/react-fontawesome":
        "commonjs @fortawesome/react-fontawesome",
      "@fortawesome/free-solid-svg-icons":
        "commonjs @fortawesome/free-solid-svg-icons",
      "@fortawesome/fontawesome-svg-core":
        "commonjs @fortawesome/fontawesome-svg-core",
      "react-redux": "commonjs react-redux",
      "react-dom": "commonjs react-dom",
      "react-router-dom": "commonjs react-router-dom",
      "redux-saga": "commonjs redux-saga",
      moment: "commonjs moment",
      mousetrap: "commonjs mousetrap",
      "react-color": "commonjs react-color",
      "react-day-picker": "commonjs react-day-picker",
      "react-markdown": "commonjs react-markdown",
      "react-slider": "commonjs react-slider",
      "react-spinners-css": "commonjs react-spinner-css",
      "rehype-raw": "commonjs rehype-raw",
      "@semantic-release/git": "commonjs semantic-release/git",
      "@semantic-release/gitlab": "commonjs semantic-release/gitlab",
      "semantic-release": "commonjs semantic-release",
      "core-js": "commonjs core-js",
    },
    devtool: mode === "development" ? "inline-source-map" : false,
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: path.join(__dirname, "src"),
          exclude: path.join(__dirname, "/node_modules/"),
          loader: "babel-loader",
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
        },
      ],
    },
  };
};

/*
  //  https://medium.com/hackernoon/the-100-correct-way-to-split-your-chunks-with-webpack-f8a9df5b7758
  optimization: {
    runtimeChunk: "single",
    splitChunks: {
      chunks: "all",
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            const packageName = module.context.match(
              /[\\/]node_modules[\\/](.*?)([\\/]|$)/
            )[1];

            // npm package names are URL-safe, but some servers don't like @ symbols
            return `npm.${packageName.replace("@", "")}`;
          },
        },
      },
    },
  },*/
