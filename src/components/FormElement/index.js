import React from "react";
import PropTypes from "prop-types";
import * as Field from "./Field";
import { FIELDTYPE } from "../../const";

const FormElement = (props) => {
  const renderField = (props) => {
    switch (props.type) {
      case FIELDTYPE.COORDINATE:
        return <Field.Coordinates {...props} />;
      case FIELDTYPE.TAGLIST:
        return <Field.Taglist {...props} />;
      case FIELDTYPE.DATE:
        return <Field.Date {...props} />;
      case FIELDTYPE.SELECTION:
        return <Field.Selection {...props} />;
      case FIELDTYPE.COLOR:
        return <Field.Color {...props} />;
      case FIELDTYPE.SLIDER:
        return <Field.Slider {...props} />;
      case FIELDTYPE.TEXTAREA:
        return <Field.Textarea {...props} />;
      case FIELDTYPE.SELECT:
        return <Field.Select {...props} />;
      case FIELDTYPE.CHECKBOX_ICON:
        return <Field.CheckboxIcon {...props} />;
      case FIELDTYPE.CHECKBOX:
        return <Field.Checkbox {...props} />;
      case FIELDTYPE.HIDDEN:
        return <Field.Hidden {...props} />;
      case FIELDTYPE.EMAIL:
        return <Field.Email {...props} />;
      case FIELDTYPE.TEXT:
        return <Field.Text {...props} />;
      case FIELDTYPE.PASSWORD:
        return <Field.Password {...props} />;

      case FIELDTYPE.DURATION:
        return <Field.Duration {...props} />;
      default: {
        return <Field.Text {...props} />;
      }
    }
  };
  if (props.isDisabled && props.value?.length === 0) return null;
  return <div>{renderField(props)}</div>;
};
export default FormElement;

FormElement.defaultProps = {
  hasOptionToggles: false,
};

FormElement.propTypes = {
  hasOptionToggles: PropTypes.bool,
  styles: PropTypes.object,
  type: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.any,
  value: PropTypes.any,
  options: PropTypes.object,
  onChange: PropTypes.func,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
  isPublished: PropTypes.bool,
  isMarkdown: PropTypes.bool,
};
