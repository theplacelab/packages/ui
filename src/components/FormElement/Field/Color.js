import React, { useState } from "react";
import PropTypes from "prop-types";
import { SketchPicker } from "react-color";
import * as common from "./common";

const hexToRgb = (hex = "FFFFFF") => {
  // https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  });

  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : null;
};

const Color = ({
  value,
  isLocked = false,
  isLockable = false,
  isDisabled,
  isPublished,
  options,
  onChange,
  id,
  data,
  style,
  label,
}) => {
  const [locked, setLocked] = useState(isLocked);
  const styles = {
    label: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
    color: {
      width: "36px",
      height: "14px",
      borderRadius: "2px",
      backgroundColor: "transparent",
      border: "1px solid #e4e4e4",
    },
    colorSwatch: {
      padding: "5px",
      background: "#ffffff",
      borderRadius: ".25px",
      boxShadow: "0 0 0 1px rgba(0, 0, 0, 0.1)",
      display: "inline-block",
      cursor: "pointer",
      border: "1px solid #767676",
    },
    colorPopover: { position: "relative", zIndex: "1" },
    colorCover: {
      position: "fixed",
      top: "0px",
      right: "0px",
      bottom: "0px",
      left: "0px",
    },
    ...style,
  };
  value = typeof value === "object" ? value.color : value;

  const colorRGB = hexToRgb(value)
    ? hexToRgb(value)
    : { r: 0, g: 0, b: 0, a: 0 };
  isDisabled = isDisabled || locked;
  const [displayColorPicker, setDisplayColorPicker] = useState(false);
  options = {
    alpha: 255,
    ...options,
  };

  const [colorpicker, setColorpicker] = useState({
    sendOpacity: false,
    color: {
      r: colorRGB.r,
      g: colorRGB.g,
      b: colorRGB.b,
      a: options.alpha,
    },
  });

  const onClick = () => {
    if (!isDisabled) setDisplayColorPicker(!displayColorPicker);
  };

  const onClose = () => {
    setDisplayColorPicker(false);
  };

  const onChangeHandler = (color) => {
    setColorpicker({ ...colorpicker, color: color.rgb });
    onChange({
      id: id,
      value: {
        color: color.hex,
        opacity: color.rgb.a === 0 ? 0.1 : color.rgb.a,
      },
      data: options?.data ? options?.data : data,
    });
  };

  const colorSwatchStyle = {
    ...styles.colorSwatch,
    background: isDisabled ? "#f8f8f8" : styles.colorSwatch.background,
    border: isDisabled ? ".25px #d1d1d1 solid" : styles.colorSwatch.border,
    boxShadow: isDisabled ? "" : styles.colorSwatch.boxShadow,
  };

  return (
    <div>
      <div style={{ ...styles.label, marginBottom: ".52rem" }}>{label}</div>
      <div
        style={{ display: "flex", alignItems: "center", flexDirection: "row" }}
      >
        {isLockable && (
          <div onClick={() => setLocked(!locked)}>
            <div
              style={common.styles.lock}
              className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
            />
          </div>
        )}
        <div style={colorSwatchStyle} onClick={onClick}>
          <div>
            <div
              style={{
                ...styles.color,
                backgroundColor: value,
              }}
            />
          </div>
        </div>
        {displayColorPicker ? (
          <div style={styles.colorPopover}>
            <div style={styles.colorCover} onClick={onClose} />
            <SketchPicker
              color={colorpicker.color}
              onChange={(color) =>
                onChangeHandler(color, colorpicker.sendOpacity)
              }
            />
          </div>
        ) : null}
      </div>
    </div>
  );
};

Color.defaultProps = {
  onChange: () => {},
  isPublished: true,
};

Color.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
  isPublished: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.object,
  value: PropTypes.any,
};

export default Color;
