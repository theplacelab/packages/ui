import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import * as common from "./common";

const isJSON = (str) => {
  try {
    return JSON.parse(str) && !!str;
  } catch (e) {
    return false;
  }
};

const Textarea = ({
  isPublished = true,
  isMarkdown = true,
  onChange,
  onBlur,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  const styles = {
    label: common.styles.label_col,
    textarea: {
      borderRadius: `0.3rem`,
      border: `1px solid`,
      margin: "0 1rem 0 0.1rem",
      padding: "0.5rem 0.5rem 0.5rem 0.5rem",
      background: "white",
      width: "20rem",
      height: "10rem",
      maxHeight: "100%",
    },
    textarea_disabled: {
      color: "#747474",
      width: "20rem",
      height: "11rem",
      maxHeight: "100%",
      overflow: "scroll",
      background: "rgb(250, 250, 250)",
      borderRadius: `0.3rem`,
      border: `1px solid #d3d3d3`,
      padding: "0 1rem",
    },
    ...style,
  };
  isDisabled = isDisabled || locked;
  const content = value === null ? "" : value;
  let hasValidation = options?.validation && props;
  const [isValid, setIsValid] = useState(hasValidation ? true : null);
  const [fieldValue, setFieldValue] = useState(value);
  const onChangeHandler = (e) => {
    let value = e.target.value;
    setFieldValue(value);
    setIsValid(!hasValidation);
    if (hasValidation) {
      if (!isJSON(value)) {
        return null;
      } else {
        setIsValid(true);
        value = JSON.parse(value);
      }
    }
    onChange({
      id: e.target.dataset.id,
      value,
      data: options?.data ? options?.data : data,
      type: "change",
    });
  };
  const onBlurHandler = (e) => {
    if (isLockable) {
      setLocked(true);
    } else {
      if (hasValidation && !isValid) setFieldValue(value);
    }
    let value = e.target.value;
    setFieldValue(value);
    setIsValid(!hasValidation);
    if (hasValidation) {
      if (!isJSON(value)) {
        return null;
      } else {
        setIsValid(true);
        value = JSON.parse(value);
      }
    }
    onBlur({
      id: e.target.dataset.id,
      value,
      data: options?.data ? options?.data : data,
      type: "blur",
    });
  };

  useEffect(() => {
    setFieldValue(value);
  }, [value]);

  const icon = isValid ? (
    <div className="fas fa-check" style={{ color: "#bada55" }} />
  ) : (
    <div className="fas fa-times" style={{ color: "#ff0000" }} />
  );
  return (
    <label style={styles.label}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <div style={{ flexGrow: 1 }}>{label === null ? "" : label}</div>
      </div>
      <div>
        {(isDisabled && (
          <div
            style={styles.textarea_disabled}
            onClick={() => {
              if (isLockable) setLocked(false);
            }}
          >
            {(isMarkdown && (
              <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                {content}
              </ReactMarkdown>
            )) ||
              content}
          </div>
        )) || (
          <div>
            <textarea
              style={styles.textarea}
              type={"textarea"}
              autoComplete="off"
              disabled={isDisabled}
              data-id={id}
              value={fieldValue}
              onBlur={onBlurHandler}
              onChange={onChangeHandler}
            />
            <div style={{ textAlign: "center" }}>
              {isValid !== null && <div style={{ width: "1rem" }}>{icon}</div>}
            </div>
          </div>
        )}
      </div>
    </label>
  );
};

Textarea.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  locked: PropTypes.bool,
  isMarkdown: PropTypes.bool,
  isPublished: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.any,
  value: PropTypes.any,
};

export default Textarea;
