import React from "react";
import PropTypes from "prop-types";

const Hidden = ({ id, value }) => {
  return (
    <input
      type={"hidden"}
      autoComplete="off"
      data-id={id}
      value={value === null ? "" : value}
    />
  );
};

Hidden.propTypes = {
  onChange: PropTypes.func,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
  id: PropTypes.any,
  value: PropTypes.any,
  options: PropTypes.object,
};

export default Hidden;
