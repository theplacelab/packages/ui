import React, { useState } from "react";
import PropTypes from "prop-types";
import { useEffect } from "react";
import * as common from "./common";

const Text = ({
  isPublished = true,
  onChange,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
  onBlur,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  isDisabled = isDisabled || locked;
  const [fieldValue, setFieldValue] = useState(
    !value || value === null ? "" : value
  );

  useEffect(() => {
    setFieldValue(value);
  }, [value]);

  let placeholder = "";
  if (options?.validation) {
    placeholder = `${options.validation.min} ${
      options.validation.max ? ` - ${options.validation.max}` : ""
    }`;
  }
  const validate = (val) => {
    let newVal = val;
    switch (options.validation.type) {
      case "integer": {
        if (val && val !== "-") {
          newVal = parseInt(val, 10);
          newVal = isNaN(newVal) ? 0 : newVal;

          if (typeof options.validation.min !== "undefined")
            newVal =
              parseInt(newVal) <= options.validation.min
                ? fieldValue === "-"
                  ? options.validation.min
                  : fieldValue
                : newVal;

          if (typeof options.validation.max !== "undefined")
            newVal =
              parseInt(newVal) >= options.validation.max ? fieldValue : newVal;
        }
        setFieldValue(newVal);
        break;
      }
      case "float": {
        let endsInPeriod = val.charAt(val.length - 1) === ".";
        let newVal = val;
        if (val && val !== "-") {
          newVal = parseFloat(val);
          newVal = isNaN(newVal) ? 0.0 : `${newVal}${endsInPeriod ? "." : ""}`;
        }
        setFieldValue(newVal);
        break;
      }
      default:
        break;
    }
    return isNaN(newVal) ? 0.0 : newVal;
  };
  const onChangeHandler = (e) => {
    if (!options?.validation) setFieldValue(e.target.value);

    if (typeof onChange === "function")
      onChange({
        id: e.target.dataset.id,
        value: options?.validation ? validate(e.target.value) : e.target.value,
        data: options?.data,
        type: "change",
      });
  };
  const onBlurHandler = (e) => {
    if (typeof onBlur === "function")
      onBlur({
        id: e.target.dataset.id,
        value: options?.validation ? validate(e.target.value) : e.target.value,
        data: options?.data,
        type: "blur",
      });
  };
  const styles = {
    label: common.styles.label_col,
    lockableRow: common.styles.lockableRow,
    ...style,
  };
  let inputProps = {
    placeholder,
    style: styles.input,
    type: "text",
    autoComplete: "off",
    disabled: isDisabled || locked,
    "data-id": id,
    value: fieldValue,
    onChange: onChangeHandler,
    onBlur: onBlurHandler,
  };
  if (options?.data)
    Object.keys(options.data).forEach((item) => {
      inputProps[`data-${item.toLowerCase()}`] = options.data[item];
    });
  inputProps.value = inputProps.value ? inputProps.value : "";
  return (
    <label style={styles.label}>
      <div
        title={tooltip}
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <div style={{ flexGrow: 1 }}>{label === null ? "" : label}</div>
      </div>
      <div style={styles.lockableRow}>
        {isLockable && (
          <div onClick={() => setLocked(!locked)}>
            <div
              style={common.styles.lock}
              className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
            />
          </div>
        )}
        <div>
          <input {...inputProps} />
        </div>
      </div>
    </label>
  );
};

Text.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  locked: PropTypes.bool,
  isPublished: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.shape({
    input: PropTypes.any,
    label: PropTypes.any,
  }),
  tooltip: PropTypes.any,
  value: PropTypes.any,
};

export default Text;
