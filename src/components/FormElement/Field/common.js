export const styles = {
  label_row: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  label_col: {
    display: "flex",
    flexDirection: "column",
    alignItems: "left",
  },
  lock: {
    opacity: "0.5",
    fontSize: ".8rem",
    marginRight: ".5rem",
  },
  lockableRow: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    maxWidth: "100%",
  },
};
