import React, { useState } from "react";
import PropTypes from "prop-types";
import * as common from "./common";

const Select = ({
  isPublished = true,
  onChange,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  isDisabled = isDisabled || locked;
  const styles = {
    label: common.styles.label_row,
    selectInput: {
      width: "100%",
      border: `0.1px solid #767676`,
      borderRadius: "0.2rem",
      fontSize: "1rem",
    },
    selectInput_disabled: {
      width: "100%",
      border: `0.1px solid lightgrey`,
      borderRadius: "0.2rem",
      fontSize: "1rem",
      opacity: 1,
      backgroundColor: "rgb(250, 250, 250)",
    },
    lockableRow: common.styles.lockableRow,
    ...style,
  };

  return (
    <div title={tooltip}>
      <div style={styles.label}>{label}</div>
      <div style={styles.lockableRow}>
        {isLockable && (
          <div onClick={() => setLocked(!locked)}>
            <div
              style={common.styles.lock}
              className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
            />
          </div>
        )}
        <div>
          <select
            style={
              isDisabled ? styles.selectInput_disabled : styles.selectInput
            }
            value={value}
            onChange={(e) => {
              let item = options?.selectionOptions.find(
                (item) => item.id === parseInt(e.target.value, 10)
              );
              onChange({
                id: id,
                value: e.target.value,
                data: {
                  ...options?.data,
                  item,
                },
              });
            }}
            autoComplete={id}
            disabled={isDisabled}
            data-id={id}
          >
            {options?.selectionOptions?.map((item, idx) => {
              return (
                <option key={`select_${id}_${idx}`} value={item.id}>
                  {item.name}
                </option>
              );
            })}
          </select>
        </div>
      </div>
    </div>
  );
};

Select.propTypes = {
  label: PropTypes.string,
  id: PropTypes.any,
  value: PropTypes.any,
  options: PropTypes.object,
  onChange: PropTypes.func,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
};

export default Select;
