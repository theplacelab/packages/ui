import React, { useState } from "react";
import PropTypes from "prop-types";
import * as common from "./common";

const Taglist = ({
  isPublished = true,
  onChange,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
  onTagClick,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  onTagClick = onTagClick
    ? onTagClick
    : () =>
        console.warn(
          "No tag click function defined and taglist isn't click-to-edit (lockable)"
        );
  isDisabled = isDisabled || locked;
  const styles = {
    label: common.styles.label_col,
    tag: {
      height: "1rem",
      fontSize: "1rem",
      lineHeight: "0.9rem",
      margin: ".5rem .1rem 0 .1rem",
      color: "#ffffff",
      padding: "0.25rem 0.5rem",
      backgroundColor: "grey",
      borderRadius: ".8rem",
    },
    tagCollection: {
      height: "2.5rem",
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
    ...style,
  };

  if (isDisabled || locked) {
    let tag_output = [];
    let tags = value.split(",");
    for (let idx = 0; idx < tags.length; idx++) {
      let tag = tags[idx].trim();
      tag_output.push(
        <div
          key={`tag_${tag}`}
          style={styles.tag}
          onClick={(e) => {
            if (isLockable) {
              setLocked(false);
            } else {
              onTagClick(e);
            }
          }}
        >
          {tag}
        </div>
      );
    }
    return (
      <label style={styles.label}>
        <div>{label === null ? "" : label}</div>
        <div>
          <div style={styles.tagCollection}>{tag_output}</div>
        </div>
      </label>
    );
  } else {
    return (
      <label style={styles.label}>
        <div>{label === null ? "" : label}</div>
        <div>
          <input
            style={styles.input}
            type="text"
            autoComplete="off"
            disabled={isDisabled || locked}
            data-id={id}
            value={value === null ? "" : value}
            onBlur={() => {
              if (isLockable) setLocked(true);
            }}
            onChange={(e) =>
              onChange({
                id: e.target.dataset.id,
                value: e.target.value,
                data: options?.data,
              })
            }
          />
        </div>
      </label>
    );
  }
};

Taglist.propTypes = {
  label: PropTypes.string,
  onChange: PropTypes.func,
  isDisabled: PropTypes.bool,
  locked: PropTypes.bool,
  id: PropTypes.any,
  value: PropTypes.any,
  options: PropTypes.object,
};

export default Taglist;
