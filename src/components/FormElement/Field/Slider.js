import React, { useState } from "react";
import PropTypes from "prop-types";
import * as common from "./common";

const Slider = ({
  isPublished = true,
  onChange,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  isDisabled = isDisabled || locked;
  const styles = {
    label: common.styles.label_col,
    lockableRow: common.styles.lockableRow,
    ...style,
  };
  return (
    <label style={styles.label}>
      <div>{label === null ? "" : label}</div>
      <div style={styles.lockableRow}>
        {isLockable && (
          <div onClick={() => setLocked(!locked)}>
            <div
              style={common.styles.lock}
              className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
            />
          </div>
        )}
        <input
          min={options.min ? options.min : 0}
          max={options.max ? options.max : 1}
          step={options.step ? options.step : 0.1}
          style={styles.input}
          type="range"
          autoComplete="off"
          disabled={isDisabled || locked}
          data-id={id}
          value={value}
          onChange={(e) =>
            onChange({
              id: e.target.dataset.id,
              value: e.target.value,
              data: options?.data,
            })
          }
        />
      </div>
    </label>
  );
};

Slider.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  locked: PropTypes.bool,
  isPublished: PropTypes.any,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.any,
  value: PropTypes.any,
};

export default Slider;
