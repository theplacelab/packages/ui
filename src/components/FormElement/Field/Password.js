import React, { useState } from "react";
import PropTypes from "prop-types";
import * as common from "./common";

const Password = ({
  isPublished = true,
  onChange,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  const styles = {
    label: common.styles.label_col,
    lockableRow: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      maxWidth: "100%",
    },
    ...style,
  };

  return (
    <label style={styles.label}>
      <div>{label === null ? "" : label}</div>
      <div style={styles.lockableRow}>
        {isLockable && (
          <div onClick={() => setLocked(!locked)}>
            <div
              style={common.styles.lock}
              className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
            />
          </div>
        )}
        <input
          style={styles.input}
          type="password"
          autoComplete="off"
          disabled={isDisabled || locked}
          data-id={id}
          value={value === null ? "" : value}
          onChange={(e) =>
            onChange({
              id: e.target.dataset.id,
              value: e.target.value,
              data: options?.data ? options.data : {},
            })
          }
        />
      </div>
    </label>
  );
};

Password.defaultProps = {
  onChange: () => {},
};

Password.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
  isPublished: PropTypes.any,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.any,
  value: PropTypes.any,
};

export default Password;
