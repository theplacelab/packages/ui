import React, { useState, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import Mousetrap from "mousetrap";
import * as common from "./common";

const Duration = ({
  isPublished = true,
  onChange,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  const styles = {
    input: { width: "94%" },
    label: common.styles.label_col,
    lockableRow: common.styles.lockableRow,
    ...style,
  };
  const [minutes, setMinutes] = useState("");
  const [seconds, setSeconds] = useState("");
  const minutesRef = useRef();
  const secondsRef = useRef();

  const parseValues = (min, sec) => {
    let minutes = parseInt(min, 10);
    let seconds = parseInt(sec, 10);
    minutes = isNaN(minutes) ? 0 : minutes;
    seconds = isNaN(seconds) ? 0 : seconds;
    minutes = seconds > 60 ? 0 : minutes;
    let totalSeconds = seconds + minutes * 60;
    minutes = Math.floor(totalSeconds / 60);
    seconds = totalSeconds - minutes * 60;
    setMinutes(minutes.toString().padStart(2, "0"));
    setSeconds(seconds.toString().padStart(2, "0"));
  };

  const onChangeHandler = () => {
    parseValues(minutesRef.current.value, secondsRef.current.value);
    let seconds =
      parseInt(minutesRef.current.value, 10) * 60 +
      parseInt(secondsRef.current.value, 10);
    onChange({
      id: id,
      value: seconds * 1000,
      data: {
        ...(options?.data ? options.data : {}),
        minutes: minutesRef.current.value,
        seconds: secondsRef.current.value,
      },
    });
  };

  const handleArrow = (e) => {
    let ref = e.target.dataset.id === "minutes" ? minutesRef : secondsRef;
    let currentVal = parseInt(ref.current.value, 10);
    currentVal = isNaN(currentVal) ? 0 : currentVal;
    ref.current.value =
      e.key === "ArrowDown"
        ? currentVal - 1 >= 0
          ? currentVal - 1
          : 0
        : currentVal + 1;
    onChangeHandler(e);
  };

  useEffect(() => {
    let sec = value / 1000;
    let min = sec >= 60 ? Math.floor(sec / 60) : 0;
    sec = min > 0 ? sec - min * 60 : sec;
    parseValues(min, sec);
  }, [value]);

  return (
    <label style={styles.label}>
      <div
        title={tooltip}
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "left",
        }}
      >
        <div style={{ flexGrow: 1 }}>{label}</div>
      </div>
      <div>
        <div style={styles.lockableRow}>
          {isLockable && (
            <div onClick={() => setLocked(!locked)}>
              <div
                style={common.styles.lock}
                className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
              />
            </div>
          )}
          <div style={{ flexGrow: 0.5 }}>
            <input
              className="mousetrap"
              onFocus={() => Mousetrap.bind(["up", "down"], handleArrow)}
              onBlur={() => Mousetrap.unbind(["up", "down"], handleArrow)}
              ref={minutesRef}
              placeholder={"MM"}
              style={styles.input}
              type="text"
              autoComplete="off"
              disabled={isDisabled || locked}
              data-id={"minutes"}
              value={minutes}
              onChange={onChangeHandler}
            />
          </div>
          <div style={{ textAlign: "center", width: "1rem" }}>:</div>
          <div style={{ flexGrow: 0.5 }}>
            <input
              className="mousetrap"
              onFocus={() => Mousetrap.bind(["up", "down"], handleArrow)}
              onBlur={() => Mousetrap.unbind(["up", "down"], handleArrow)}
              ref={secondsRef}
              placeholder={"SS"}
              style={styles.input}
              type="text"
              autoComplete="off"
              disabled={isDisabled || locked}
              data-id={"seconds"}
              value={seconds}
              onChange={onChangeHandler}
            />
          </div>
        </div>
      </div>
    </label>
  );
};

Duration.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  locked: PropTypes.bool,
  isPublished: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.object,
  tooltip: PropTypes.any,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default Duration;
