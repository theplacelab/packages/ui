import React, { useState } from "react";
import PropTypes from "prop-types";
import * as common from "./common";

const CheckboxIcon = ({
  options,
  isDisabled,
  isLocked = false,
  isLockable = false,
  label,
  isPublished = true,
  onChange,
  style,
  value,
  id,
  data
}) => {
  const opts = {
    activeColor: "rgb(194, 255, 0)",
    showBothActive: false,
    invert: false,
    ...options
  };

  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  isDisabled = isDisabled || locked;

  let color = opts.invert
    ? value
      ? opts.showBothActive
        ? opts.activeColor
        : "grey"
      : opts.activeColor
    : value
    ? opts.activeColor
    : opts.showBothActive
    ? opts.activeColor
    : "grey";

  const altIcon = opts.iconAlt ? opts.iconAlt : opts.icon;
  let icon = opts.invert
    ? value
      ? altIcon
      : opts.icon
    : value
    ? opts.icon
    : altIcon;
  icon = icon ? icon : "fas fa-check";

  const styles = {
    label: common.styles.label_row,
    icon: {
      padding: ".2rem",
      fontSize: "2rem",
      color,
      opacity: isDisabled ? 0.5 : 1,
      border: isDisabled ? ".25px #d1d1d1 solid" : "",
      borderRadius: "0.3rem",
      backgroundColor: isDisabled ? "#f8f8f8" : ""
    },
    ...style
  };

  return (
    <div>
      <div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            width: "100%",
            alignItems: "center"
          }}
        >
          <div>
            {isLockable && (
              <div onClick={() => setLocked(!locked)}>
                <div
                  style={common.styles.lock}
                  className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
                />
              </div>
            )}
            <div
              style={styles.icon}
              className={icon}
              checked={value ? true : false}
              data-id={id}
              onClick={(e) => {
                if (!isDisabled && !locked)
                  onChange({
                    id: e.target.dataset.id,
                    value: value ? false : true,
                    data: options?.data ? options?.data : data,
                    type: "change"
                  });
              }}
            />
          </div>
          <div style={{ flexGrow: 1, marginLeft: ".25rem" }}>
            {options.text}
          </div>
        </div>
      </div>
    </div>
  );
};

CheckboxIcon.propTypes = {
  activeColor: PropTypes.string,
  id: PropTypes.any,
  invert: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
  isPublished: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  showBothActive: PropTypes.bool,
  style: PropTypes.any,
  value: PropTypes.any
};

export default CheckboxIcon;
