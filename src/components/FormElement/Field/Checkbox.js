import React, { useState } from "react";
import PropTypes from "prop-types";
import * as common from "./common";

const Checkbox = ({
  isPublished = true,
  onChange,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  const styles = {
    label: common.styles.label_row,
    checkboxText: {
      fontSize: "1rem",
      marginLeft: ".5rem",
    },
    ...style,
  };

  return (
    <div title={tooltip}>
      <div style={styles.label}>
        {isLockable && (
          <div onClick={() => setLocked(!locked)}>
            <div
              style={common.styles.lock}
              className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
            />
          </div>
        )}
        <label style={styles.label}>
          <div>
            <input
              disabled={isDisabled || locked}
              type="checkbox"
              autoComplete="off"
              checked={value ? true : false}
              data-id={id}
              onChange={(e) =>
                onChange({
                  id: e.target.dataset.id,
                  value: value ? false : true,
                  data: options?.data ? options?.data : data,
                })
              }
            />
          </div>
          <div style={styles.checkboxText}>{label === null ? "" : label}</div>
        </label>
      </div>
    </div>
  );
};

Checkbox.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
  isPublished: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.any,
  tooltip: PropTypes.string,
  value: PropTypes.any,
};

export default Checkbox;
