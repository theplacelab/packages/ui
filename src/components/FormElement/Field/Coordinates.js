import React, { useState } from "react";
import PropTypes from "prop-types";
import * as common from "./common";

const Coordinates = ({
  style,
  id,
  value,
  options,
  data,
  isDisabled,
  isPublished,
  isLocked = false,
  isLockable = false,
  onChange,
  label,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  const styles = {
    label: common.styles.label_col,
    ...style,
  };
  const [isValid, setIsValid] = useState(null);
  value = typeof value === "object" ? value : { lat: 0, lng: 0 };
  const [fieldValue, setFieldValue] = useState(
    `${value?.lat ? value.lat : 0}, ${value?.lng ? value.lng : 0}`
  );
  const icon = isValid ? (
    <div className="fas fa-check" style={{ color: "#bada55" }} />
  ) : (
    <div className="fas fa-times" style={{ color: "#ff0000" }} />
  );
  const onValidate = (e) => {
    setFieldValue(e.target.value);
    setIsValid(false);
    try {
      let coordinates = e.target.value.split(",");
      let lat = parseFloat(coordinates[0]);
      let lng = parseFloat(coordinates[1]);
      if (lat >= -90 && lat <= 90 && lng >= -180 && lng <= 180) {
        let coordinates = { lat, lng };
        console.log(coordinates);
        setIsValid(true);
        setFieldValue(`${lat}, ${lng}`);
        onChange({
          id: id,
          value: coordinates,
          data: options?.data ? options?.data : data,
        });
      }
    } catch (error) {
      //Noop
    }
  };
  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          width: "100%",
        }}
      >
        <label style={styles.label}>
          <div>{label === null ? "" : label}</div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "row",
            }}
          >
            {isLockable && (
              <div onClick={() => setLocked(!locked)}>
                <div
                  style={common.styles.lock}
                  className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
                />
              </div>
            )}
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                width: "100%",
              }}
            >
              <input
                disabled={isDisabled || locked}
                style={styles.input}
                onChange={onValidate}
                value={fieldValue}
                onBlur={(e) => {
                  if (!isValid) {
                    e.target.value = value
                      ? `${value?.lat}, ${value.lng}`
                      : "0,0";
                    onValidate(e);
                  }
                }}
              />
              <div style={{ textAlign: "center" }}>
                {isValid !== null && (
                  <div style={{ width: "1rem" }}>{icon}</div>
                )}
              </div>
            </div>
          </div>
        </label>
      </div>
    </div>
  );
};

Coordinates.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
  isPublished: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.any,
  value: PropTypes.shape({
    lat: PropTypes.any,
    lng: PropTypes.any,
  }),
};

export default Coordinates;
