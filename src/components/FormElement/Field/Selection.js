import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import WatchForOutsideClick from "../../WatchForOutsideClick";
import * as common from "./common";

const Selection = ({
  isPublished = true,
  onChange,
  isDisabled,
  isLocked = false,
  isLockable = false,
  options,
  data,
  label,
  tooltip,
  style,
  value,
  id,
}) => {
  const [locked, setLocked] = useState(isLocked);
  onChange = onChange ? onChange : () => {};
  isDisabled = isDisabled || locked;
  const [displayValue, setDisplayValue] = useState({ id: null, name: "" });
  const [selectionVisible, setSelectionVisible] = useState(false);
  const [currentValue, setCurrentValue] = useState({ id: null, name: "" });
  const [hoverOn, setHoverOn] = useState(false);

  useEffect(() => {
    const currentItem = options.selectionOptions.find(
      (item) => item.id === parseInt(value, 10)
    );
    setCurrentValue(currentItem);
    setDisplayValue(currentItem);
  }, [value]);

  const onSelect = (e) => {
    e.preventDefault();
    const payload = {
      id,
      value: e.target.dataset.id,
      data: {
        ...options?.data,
        item: options.selectionOptions.find(
          (item) => item.id === parseInt(e.target.dataset.id, 10)
        ),
      },
    };
    setSelectionVisible(false);
    onChange(payload);
  };

  const onBlur = (e) => {
    if (displayValue.id === null) {
      const payload = {
        id,
        value: null,
        data: {
          ...options?.data,
          item: displayValue,
        },
      };
      onChange(payload);
    }
  };

  const styles = {
    label: common.styles.label_col,
    lockableRow: common.styles.lockableRow,
    selectionDropdown: {
      zIndex: "1",
      position: "relative",
      backgroundColor: "#ffffff",
      width: "100%",
      border: ".25px solid lightgrey",
      boxShadow: "#000000",
      overflow: "hidden",
      display: "block",
      borderRadius: `0 0 0.3rem 0.3rem`,
    },
    selectionDropdownItem: {
      padding: "0.2rem 0 0.2rem 0.4rem",
    },
    selectionDropdownItem_hover: {
      padding: "0.2rem 0 0.2rem 0.4rem",
      backgroundColor: "grey",
      color: "white",
    },
    chevron: {
      fontWeight: 900,
      fontSize: "1.4rem",
      marginLeft: "-1rem",
      marginTop: "-0.9rem",
      opacity: isDisabled ? 0.5 : 1,
    },
    row: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
    ...style,
  };
  let dropdown_id = `selection_dropdown_${id}`;

  return (
    <label style={styles.label}>
      <div>{label === null ? "" : label}</div>
      <div style={styles.lockableRow}>
        {isLockable && (
          <div
            onClick={(e) => {
              e.preventDefault();
              setLocked(!locked);
            }}
          >
            <div
              style={common.styles.lock}
              className={`fas ${locked ? "fa-lock" : "fa-lock-open"}`}
            />
          </div>
        )}
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div style={styles.row}>
            <div>
              <input
                style={{ fontSize: "1rem" }}
                autoComplete="off"
                disabled={isDisabled || locked}
                data-id={id}
                type="text"
                value={displayValue.name}
                onClick={() => setSelectionVisible(true)}
                onBlur={onBlur}
                onChange={(e) => {
                  if (options.acceptsNew)
                    setDisplayValue({ id: null, name: e.currentTarget.value });
                }}
              />
            </div>
            <div style={styles.chevron}>&#8964;</div>
          </div>

          <WatchForOutsideClick
            onOutsideClick={() => setSelectionVisible(false)}
          >
            <div
              id={dropdown_id}
              style={{
                ...styles.selectionDropdown,
                display: selectionVisible ? "block" : "none",
              }}
            >
              {options.selectionOptions &&
                options.selectionOptions.map((item, idx) => {
                  const style =
                    hoverOn === idx
                      ? styles.selectionDropdownItem_hover
                      : styles.selectionDropdownItem;
                  return (
                    <div
                      onMouseEnter={() => setHoverOn(idx)}
                      onTouchStart={() => setHoverOn(idx)}
                      onTouchCancel={() => setHoverOn(null)}
                      onTouchEnd={() => setHoverOn(null)}
                      onMouseLeave={() => setHoverOn(null)}
                      key={`${id}_${idx}`}
                      data-id={item.id}
                      onClick={onSelect}
                      style={style}
                    >
                      {item.name}
                    </div>
                  );
                })}
            </div>
          </WatchForOutsideClick>
        </div>
      </div>
    </label>
  );
};

Selection.propTypes = {
  id: PropTypes.any,
  isDisabled: PropTypes.bool,
  isLockable: PropTypes.bool,
  isPublished: PropTypes.any,
  label: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object,
  style: PropTypes.object,
  value: PropTypes.any,
};

export default Selection;
