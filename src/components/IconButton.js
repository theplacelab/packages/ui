import React, { useState } from "react";

function IconButton({
  toolTip,
  onClick,
  style,
  isDisabled = false,
  icon = "fas fa-cat",
}) {
  const [mouseDown, setMouseDown] = useState(false);
  const styles = {
    container: { display: "flex", flexDirection: "row", alignItems: "center" },
    icon: {
      color: style?.color ? style.color : "black",
      marginRight: "1rem",
      fontSize: "1rem",
    },
    iconSelected: {
      color: style?.color ? style.color : "black",
      marginRight: "1rem",
      opacity: 0.5,
      transform: "scale(1.2, 1.2)",
    },
  };
  return (
    <div
      onMouseLeave={() => setMouseDown(false)}
      onMouseDown={() => setMouseDown(true)}
      onMouseUp={() => setMouseDown(false)}
      title={toolTip}
      onClick={(e) => {
        if (!isDisabled) if (typeof onClick === "function") onClick(e);
      }}
      className={icon}
      style={{
        ...(mouseDown
          ? isDisabled
            ? styles.icon
            : styles.iconSelected
          : styles.icon),
        opacity: isDisabled ? 0.2 : 1,
      }}
    />
  );
}
export default IconButton;
