import React, { useEffect, useState } from "react";

const SortableTable = ({ headers, data, options = {}, style }) => {
  const { searchOn, filterOn } = options;
  const [filterText, setFilterText] = useState("");
  const [tableData, setTableData] = useState(data);
  const [sortDirection, setSortDirection] = useState(true);
  const [sortOn, setSortOn] = useState(null);

  const limitColumns = 1;

  let gridTemplateColumns = "";
  headers.forEach((header) => {
    gridTemplateColumns += header.width ? `${header.width} ` : "1fr ";
  });

  const doSort = (e) => {
    const sortedData = [...tableData];
    try {
      sortedData.sort((a, b) => {
        const string_a =
          a[parseInt(e.currentTarget.dataset.id, 10)].props["data-sorttext"];
        const string_b =
          b[parseInt(e.currentTarget.dataset.id, 10)].props["data-sorttext"];
        let target_a = Array.isArray(string_a) ? string_a.join(" ") : string_a;
        let target_b = Array.isArray(string_b) ? string_b.join(" ") : string_b;
        target_a = target_a ? target_a?.toLowerCase() : "";
        target_b = target_b ? target_b?.toLowerCase() : "";
        return sortDirection
          ? target_b.localeCompare(target_a)
          : target_a.localeCompare(target_b);
      });
    } catch (e) {
      console.error(e);
    }
    setSortOn(e.currentTarget.innerText.toLowerCase());
    setSortDirection(!sortDirection);
    setTableData(sortedData);
  };

  useEffect(() => setTableData(data), [data]);
  useEffect(() => {
    if (filterText.length > 0) {
      let filteredData = [...tableData];
      filteredData = filteredData.filter((row) => {
        const target = Array.isArray(row[filterOn].props.children)
          ? row[filterOn].props.children.join(" ")
          : row[filterOn].props.children;
        return target.toLowerCase
          ? target.toLowerCase().includes(filterText.toLowerCase())
          : null;
      });
      setTableData(filteredData);
    } else {
      setTableData(data);
    }
  }, [filterText]);

  if (!headers || !data) return null;
  return (
    <div>
      {searchOn >= 0 && (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <input
            autoComplete="off"
            placeholder={`Search ${headers[parseInt(searchOn, 10)]?.label}s...`}
            value={filterText}
            type="text"
            onChange={(e) => setFilterText(e.target.value)}
            style={style?.filter}
          />
          <div onClick={() => setFilterText("")}>
            <div
              style={{ marginLeft: "-1.2rem" }}
              className={"fas fa-times-circle"}
            />
          </div>
          <div style={{ flexGrow: 1 }} />
          <div>{options.headerActions}</div>
        </div>
      )}
      <div
        style={{
          display: "grid",
          gridTemplateColumns,
          alignItems: "center",
        }}
      >
        {headers.map((header, idx) => {
          const isSelected = sortOn === header.label?.toLowerCase();
          return (
            <div
              onClick={header?.sortable ? doSort : null}
              data-id={idx}
              data-label={header?.label?.toLowerCase()}
              key={`th_${idx}`}
              style={{
                borderRight: "1px solid white",
                fontWeight: isSelected ? "900" : "200",
                ...style?.tableHeader,
                ...header?.style,
                textAlign: header?.align,
              }}
            >
              <div>
                <div
                  style={{
                    display: "inline-flex",
                    flexDirection: "row",
                  }}
                >
                  <div>{header?.label}</div>{" "}
                  <div style={{ marginLeft: ".2rem" }}>
                    <span
                      className={
                        isSelected
                          ? sortDirection
                            ? "fas fa-caret-up"
                            : "fas fa-caret-down"
                          : ""
                      }
                    />
                  </div>
                </div>
              </div>
            </div>
          );
        })}
        {tableData.length === 0 && (
          <div style={{ width: "100%", textAlign: "center" }}>None Found</div>
        )}
        {tableData.map((row) => {
          let rowdata = [];
          headers.forEach((header, idx) => {
            rowdata.push(
              <div
                key={idx}
                style={{
                  ...style?.tableData,
                  ...header?.style,
                  textAlign: header?.align,
                }}
              >
                <div style={{ display: "inline-flex" }}>{row[idx]}</div>
              </div>
            );
          });
          return rowdata;
        })}
      </div>
    </div>
  );
};

export default SortableTable;
