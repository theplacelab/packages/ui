import React, { useState } from "react";

const copyTextToClipboard = (text) => {
  var textArea = document.createElement("textarea");
  // Place in the top-left corner of screen regardless of scroll position.
  textArea.style.position = "fixed";
  textArea.style.top = 0;
  textArea.style.left = 0;

  // Ensure it has a small width and height. Setting to 1px / 1em
  // doesn't work as this gives a negative w/h on some browsers.
  textArea.style.width = "2em";
  textArea.style.height = "2em";

  // We don't need padding, reducing the size if it does flash render.
  textArea.style.padding = 0;

  // Clean up any borders.
  textArea.style.border = "none";
  textArea.style.outline = "none";
  textArea.style.boxShadow = "none";

  // Avoid flash of the white box if rendered for any reason.
  textArea.style.background = "transparent";
  textArea.value = text;

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    document.execCommand("copy");
  } catch (err) {
    console.error("Unable to copy");
    console.error(err);
  }

  document.body.removeChild(textArea);
};

function CopyToClipboard({ children, content, style }) {
  const [mouseDown, setMouseDown] = useState(false);
  const styles = {
    container: { display: "flex", flexDirection: "row", alignItems: "center" },
    copyIcon: {
      color: style?.color ? style.color : "black",
      marginRight: "1rem",
      fontSize: "1rem",
    },
    copyIconActive: {
      color: style?.color ? style.color : "black",
      marginRight: "1rem",
      opacity: 0.5,
      transform: "scale(1.2, 1.2)",
    },
    ...style,
  };
  return (
    <div style={styles.container}>
      <div
        onMouseDown={() => setMouseDown(true)}
        onMouseUp={() => setMouseDown(false)}
        onClick={() => copyTextToClipboard(content ? content : children)}
        style={mouseDown ? styles.copyIconActive : styles.copyIcon}
        className="fas fa-link"
      />
      <div>{children}</div>
    </div>
  );
}
export default CopyToClipboard;
