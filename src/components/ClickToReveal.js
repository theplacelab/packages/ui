import React, { useState } from "react";

function ClickToReveal({ children, showHint = true }) {
  const [showContent, setShowContent] = useState(false);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <div onClick={() => setShowContent(!showContent)}>
        {(showContent && children) || (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <div style={{ position: "relative", top: ".24rem" }}>
              *******************
            </div>
            {showHint && <div>(click to reveal)</div>}
          </div>
        )}
      </div>
    </div>
  );
}
export default ClickToReveal;
