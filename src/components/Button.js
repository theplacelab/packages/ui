import React, { useState } from "react";

function Button({ toolTip, onClick, style, label, isDisabled = false, data }) {
  const [mouseDown, setMouseDown] = useState(false);
  const styles = {
    container: { display: "flex", flexDirection: "row", alignItems: "center" },
    button: {
      color: style?.color ? style.color : "black",
      marginRight: "1rem",
      fontSize: "1rem",
      ...style,
    },
    buttonSelected: {
      color: style?.color ? style.color : "black",
      marginRight: "1rem",
      opacity: 0.5,
      ...style,
      transform: "scale(1.1, 1.1)",
    },
  };

  return (
    <input
      data={data}
      type="button"
      value={label}
      onMouseLeave={() => setMouseDown(false)}
      onMouseDown={() => setMouseDown(true)}
      onMouseUp={() => setMouseDown(false)}
      title={toolTip}
      onClick={(e) => {
        if (!isDisabled) if (typeof onClick === "function") onClick(e);
      }}
      style={{
        ...(mouseDown
          ? isDisabled
            ? styles.button
            : styles.buttonSelected
          : styles.button),
        opacity: isDisabled ? 0.2 : 1,
      }}
    />
  );
}
export default Button;
