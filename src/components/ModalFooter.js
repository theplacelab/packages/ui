import React from "react";
import { Button } from "..";

const ModalFooter = ({ cancel, confirm, styles }) => {
  return (
    <div
      style={{
        display: "flex",
        textAlign: "center",
        width: "100%",
        height: "4rem",
        position: "absolute",
        bottom: 0,
        ...styles.footer,
      }}
    >
      <div style={{ margin: "auto", display: "flex" }}>
        {confirm && confirm.label && (
          <Button
            style={{ minWidth: "5rem", ...styles.button }}
            label={confirm.label ? confirm.label : "Ok"}
            onClick={confirm.action}
          />
        )}
        {confirm && cancel && <div style={{ width: ".5rem" }}></div>}
        {cancel && (
          <React.Fragment>
            <Button
              style={{ minWidth: "5rem", ...styles.button }}
              label={cancel.label ? cancel.label : "Cancel"}
              onClick={cancel.action}
            />
          </React.Fragment>
        )}
      </div>
    </div>
  );
};
export default ModalFooter;
