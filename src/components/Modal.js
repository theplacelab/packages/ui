import React, { useState, useEffect } from "react";
import { WatchForOutsideClick } from "..";

function Modal({ children, onOutsideClick, style, backgroundStyle }) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "scroll";
    };
  }, []);
  return (
    <div
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: "rgb(0 0 0 / 42%)",
        height: "100%",
        width: "100%",
        overflow: "hidden",
        zIndex: 10,
        alignItems: "center",
        ...backgroundStyle,
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          height: "100vh",
          overflow: "hidden",
        }}
      >
        <div
          style={{
            width: "100vw",
            height: "100vh",
            margin: "auto",
            position: "relative",
            background: "white",
            color: "black",
            boxShadow: " 0 2px 10px 0 rgb(0 0 0 / 30%)",
            ...style,
          }}
        >
          <WatchForOutsideClick onOutsideClick={onOutsideClick}>
            {children}
          </WatchForOutsideClick>
        </div>
      </div>
    </div>
  );
}
export default Modal;
