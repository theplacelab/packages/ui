import React, { useState, useEffect } from "react";
import { Modal } from "..";
const Dialog = ({
  children,
  allowOutsideCancel,
  style,
  backgroundStyle,
  buttons = [],
  onOutsideClick,
}) => {
  const styles = {
    container: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      height: "calc(100% - 4rem)",
      padding: ".5rem",
    },
    scrollable: {
      flexGrow: 1,
      overflow: "scroll",
      margin: "1rem",
      textAlign: "left",
    },
    flexRow: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
    button: {
      marginRight: "0.5rem",
      minWidth: "5rem",
    },
    modalStyle: { height: "25rem", width: "25rem", marginTop: "10rem" },
    ...style,
  };

  return (
    <Modal
      onOutsideClick={onOutsideClick ? onOutsideClick : () => {}}
      style={styles.modalStyle}
    >
      <div style={styles.container}>
        <div style={styles.scrollable}>{children}</div>
        <div style={styles.flexRow}>
          {buttons.map((button, idx) => (
            <div key={idx} style={{ height: "2rem" }}>
              <input
                style={{
                  ...styles.button,
                  backgroundColor: button.default ? "dimgrey" : "#c3c3c3",
                }}
                type={button.default ? "submit" : "button"}
                value={button.label}
                onClick={button.action}
              />
            </div>
          ))}
        </div>
      </div>
    </Modal>
  );
};
export default Dialog;
