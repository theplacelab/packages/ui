import React, { useRef, useState, useEffect } from "react";

const Status = ({ style, clickAnywhereToDismiss = false, dismissAfter }) => {
  const [isVisible, setIsVisible] = useState(false);
  const [data, setData] = useState(true);
  const status = useRef();

  useEffect(() => {
    const onStatusUpdate = (e) => {
      setIsVisible(true);
      setData(e.data);
      if (dismissAfter)
        setTimeout(() => {
          status.current.style.opacity = 0;
          setTimeout(() => setIsVisible(false), 300);
        }, dismissAfter);
    };
    window.addEventListener("statusUpdate", onStatusUpdate);
    return () => {
      window.removeEventListener("statusUpdate", onStatusUpdate);
    };
  }, []);

  const styles = {
    container: {
      position: "fixed",
      display: isVisible ? "flex" : "none",
      alignItems: "center",
      backgroundColor: "red",
      color: "white",
      padding: "1rem",
      width: "100vw",
      top: 0,
      zIndex: 1,
      transition: "opacity 0.3s",
      ...style,
    },
  };

  return (
    <div
      ref={status}
      onClick={() => (clickAnywhereToDismiss ? setIsVisible(false) : null)}
      style={{
        ...styles.container,
        opacity: isVisible ? 1.0 : 0,
        backgroundColor: data.backgroundColor
          ? data.backgroundColor
          : styles.container.backgroundColor,
      }}
    >
      {data.icon && (
        <div style={{ width: "1rem", marginRight: "1rem" }}>
          <div className={`fas fa-${data.icon}`} />
        </div>
      )}
      <div style={{ flexGrow: 1, textAlign: "left" }}>{data.message}</div>
      {!clickAnywhereToDismiss && (
        <div
          onClick={() => setIsVisible(false)}
          style={{ width: "1rem", marginRight: "2rem" }}
        >
          <div className={`fas fa-times-circle`} />
        </div>
      )}
    </div>
  );
};

export default Status;
