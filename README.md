# Package: @theplacelab/ui

React components for UI Elements

[[README: Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)]  
[![Semantic Release Badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![GitLab Pipeline Status](https://gitlab.com/theplacelab/packages/ui/badges/master/pipeline.svg)](https://gitlab.com/theplacelab/packages/ui/-/pipelines/latest)

# Development

Use [yalc](https://github.com/wclr/yalc) to [solve issues with link](https://divotion.com/blog/yalc-npm-link-alternative-that-does-work). Install yalc, then:

In this package:

```
yalc publish
yalc push
```

Where you want to use it:

```
yalc link @theplacelab/ui
```

# How to Use

- Get a Personal Access Token from Gitlab

- Add an `.npmrc` to your project using the token in place of ~GITLAB_TOKEN~ (or alternately add this to your ~/.npmrc or use config to do that)

  ```
  @theplacelab:registry=https://gitlab.com/api/v4/packages/npm/
  //gitlab.com/api/v4/packages/npm/:_authToken="~GITLAB_TOKEN~"
  ```

  or

  ```
  npm config set @theplacelab:registry https://gitlab.com/api/v4/packages/npm/
  npm config set //gitlab.com/api/v4/packages/npm/:_authToken '~GITLAB_TOKEN~'
  ```

- `yarn add @theplacelab/ui`

# Requirements

Many of the form elements make use of FontAwesome via className. You should include it via a CDN:

```
 <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
```

# In This Package:

## FIELDTYPE

Constant, list of valid types for `<FormElement/>`

```js
export const FIELDTYPE = {
  CHECKBOX: "CHECKBOX",
  CHECKBOX_ICON: "CHECKBOX_ICON",
  COLOR: "COLOR",
  COORDINATE: "COORDINATE",
  DATE: "DATE",
  DURATION: "DURATION",
  HIDDEN: "HIDDEN",
  PASSWORD: "PASSWORD",
  SELECT: "SELECT",
  SELECTION: "SELECTION",
  SLIDER: "SLIDER",
  TAGLIST: "TAGLIST",
  TEXT: "TEXT",
  TEXTAREA: "TEXTAREA",
};
```

## `<ClickToReveal/>`

```html
<ClickToReveal showHint="{false}">This is Secret!</ClickToReveal>
```

## `<CopyToClipboard/>`

```html
<CopyToClipboard content="Some Text To Copy to the clipboard">
  Click icon to copy to clipboard
</CopyToClipboard>
```

## `<PopoverMenu/>`

```js
<PopoverMenu
  items={[
    { name: "foo", icon: "dog", action: (e) => console.log(e) },
    { name: "cat", icon: "cat", action: (e) => console.log(e) },
    { name: "trash", icon: "trash", action: (e) => console.log(e) },
  ]}
/>
```

## `<WatchForOutsideClick/>`

```js
<WatchForOutsideClick
  onOutsideClick={() => console.log("You clicked outside!")}
>
  [click outside me]
</WatchForOutsideClick>
```

## `<FormElement/>`

Consistent wrappers for various form elements, supports locking, as much as possible accepts the same kind of input and always outputs an onChange event passing back `{id:"", value:""}`. A reasonable handler for this would be to take the values and put them into a state variable, like this:

```js
const onChange = (payload) =>
  setFormValues({ ...formValues, [payload.id]: payload.value });
```

```js
<FormElement
  id="{idx}"
  data="{data}"
  label="{type}"
  type="{type}"
  value="{formValues[idx]}"
  isDisabled="{false}"
  options="{options}"
  onChange="{onChange}"
  isLockable="{true}"
  isLocked="{true}"
/>
```

## `<Status/>`

```js
const event = new Event("statusUpdate");
event.data = {
  message: "Your account is not verified",
  backgroundColor: "orange",
  icon: "cat",
};
window.dispatchEvent(event);
```
